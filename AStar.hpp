#ifndef ASTAR_H
#define ASTAR_H

#include"Node.hpp"
#include<vector>

class AStar {
public:
  void setParent(Node parent, Node child);
private:
  vector<Node> closed;
  vector<Node> open;
};

#endif // ASTAR_H
