CC = g++
CFLAGS +=-Wall -g -std=c++11 -lsfml-graphics -lsfml-system -lsfml-window
TARGET = Game

# wildcard for all object files and cpp files
# $< is the list of all dependancies
# #@ is the list of all output files
all: Game

Game: *.o
	$(CC) *.o -o $(TARGET) $(CFLAGS)

*.o: *.cpp
	$(CC) *.cpp -c -std=c++11


# %.o: %.cpp
# 	$(CC) $< -o $@

# $(TARGET): %.o
# 	$(CC) $(CFLAGS) $< -o $(TARGET)

clean:
	rm -f *.o

