#ifndef ENEMY_H
#define ENEMY_H

#include"Agent.hpp"

enum State {passive, defensive, aggresive};

class Enemy : public Agent {
public:
  Enemy(float _x=20, float _y=20);
  void update(const RenderWindow &relativeTo);
protected:

private:
  void reactToWorld(); // what should this do? what does it react to?
  // determines actions of AI
  State state;
};

#endif // ENEMY_H
