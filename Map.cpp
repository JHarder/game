#include"Map.hpp"

float tileToPix(int n) {
    return n*32+16;
}

int pixToTile(float n) {
    return (n-16)/32;
}

vector<Tile*> readMapFile(string file) {
    vector<Tile*> mapVec;
    fstream mapFile;
    mapFile.open(file.c_str(), ios::in);
    float x, y;
    char tileType;
    if(mapFile.is_open()) {
        while(mapFile >> tileType) {
            // possible later to switch on tileType to make different kinds of tiles
            if(tileType == 'T') {
                // get x and y coords from map, breaking if either cannot be parsed to a valid float
                if(mapFile >> x && mapFile >> y) {
                    cout << "Tile: x=" << x << " y=" << y << endl;
                    mapVec.push_back(new Tile(tileToPix(x),tileToPix(y)));
                } else {
                    cout << "invalid coordinates loaded from file '" << file << "'" << endl;
                    break;
                }
            } else {
                cout << "Corrupt map contents" << endl;
                break;
            }
        }
        mapFile.close();
    } else {
        cout << "Error, could not read file '" << file << "'" << endl;
    }
    return mapVec;
}

bool saveMapFile(string name, vector<Tile*> mapObject) {
    // format: T X Y T X Y.....
    fstream mapFile;
    mapFile.open(name.c_str(), ios::out); // fstream.open wont operate on strings???
    if(mapFile.is_open()) {
        for(auto tile : mapObject) {
            // do print tile to string into file
            Vector2i coords = tile->getCoords();
            int x = pixToTile(coords.x);
            int y = pixToTile(coords.y);
            mapFile << "T " << x << " " << y << " ";
        }
        mapFile << "\n";
        mapFile.close();
    } else {
        cout << "Error: could not write to file " << name << endl;
        return false;
    }
    return true;
}
