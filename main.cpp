#include<string>
#include<algorithm>
#include<functional>
#include<iostream>
#include"Player.hpp"
#include"Enemy.hpp"
#include"Projectile.hpp"
#include"Tile.hpp"
#include"Map.hpp"
#include<vector>

using namespace std;
using namespace sf;

RenderWindow window;
View camera; // this is the key to zooming, scrolling, rotating and stuff
Player *player;
vector<Enemy*> enemies;
vector<Projectile*> bullets;
vector<Tile*> mapVec;

void update() {
    player->update(window);

    if(player->bullet) {
        bullets.push_back(player->bullet);
    }

    for(auto e : enemies) {
        e->update(window);
    }

    for(auto t : mapVec) {
        t->update(window);
    }

    for(auto p: bullets) {
        p->update(window);
    }
}

void render() {
    window.clear(Color::Green);
    window.draw(player->getDrawable());

    for(auto e : enemies) {
        window.draw(e->getDrawable());
    }

    for(auto t : mapVec) {
        window.draw(t->getDrawable());
    }

    for(auto p : bullets) {
        window.draw(p->getDrawable());
    }

    window.display();
}

void clashAll(){
    for(auto e : enemies) {
        player->clash(e);
        for(auto p : bullets) {
            p->clash(e);
        }
    }

    for(auto t : mapVec) {
        player->clash(t);
    }
}


int main(void) {
    window.create(VideoMode(800, 600), "Game 0.1");
    window.setTitle("Game test");
    player = new Player(0,0);

    enemies.push_back(new Enemy(100,100));
    enemies.push_back(new Enemy(200,200));
    enemies.push_back(new Enemy(300,300));

    mapVec = readMapFile("test.map");
    saveMapFile("test2.map", mapVec);

    camera.reset(FloatRect(0, 0, 800, 600));
    window.setView(camera);
    
    Event event;
    Clock clock;
    float elapsedTime = 0;
    float rate = 1.0/60.0;
    double delta = 0;

    while(window.isOpen()) { // run loop, runs as long as game is open
        while(window.pollEvent(event)) { // loops through event stack untill stack is empty
            // figure out and act on what type of event
            switch(event.type) {
            case Event::Closed:
                window.close();
                break;

            case Event::KeyPressed: // act on specific keys
                if(event.key.code == sf::Keyboard::Escape) {
                    window.close();
                }
                if(event.key.code == sf::Keyboard::R) {
                    player->reset();
                }
                break;

            default:
                break;
            }
        }

        delta += elapsedTime/rate;
        elapsedTime = clock.getElapsedTime().asSeconds();
        while(delta >= 1.0){
            update();
            clashAll();
            delta -= 1.0;
            camera.move(1,0);
            window.setView(camera);
            // camera.rotate(1);
        }
        clock.restart();
        render();

    }
    return 0;
}
