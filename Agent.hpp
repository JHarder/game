#ifndef AGENT_H
#define AGENT_H

#include<SFML/Graphics.hpp>
#include<iostream>
#include<cmath>

#define PI 3.14159265

using namespace std;
using namespace sf;

namespace Direction {
    enum direction {left, right, none};
}

class Agent {
public:
    Agent(float _x=20, float _y=20);
    Sprite getDrawable();
    virtual void update(const RenderWindow &relativeTo);
    Vector2i getCoords();
    void clash(Agent* clasher);
    virtual void collide(Agent* smasher);
    bool checkCollide(Sprite crasher);
    void undoMove();
    Direction::direction facing;
    Direction::direction currentDir;
protected:
    int Gravity;
    int MaxGravity;
    Texture *tex;
    Sprite image;
    double x,y,dx,dy,oldx,oldy;
    int maxSpeed;
    string imageStr;

    virtual void move(const RenderWindow &relativeTo);
    // void followDirection();
};

#endif // AGENT_H
