#include"Projectile.hpp"

Projectile::Projectile(Agent* spawned, Vector2i start) {
    dy = 0;
    imageStr = "bullet.png";
    tex->loadFromFile(imageStr);
    image.setTexture(*tex, true);

    maxSpeed = 1;


    x = start.x;
    y = start.y;
    int height = spawned->getDrawable().getTextureRect().height;

    switch(spawned->facing) {
    case Direction::left:
        y-=height/2 - 4;
        facing = Direction::left;
        break;
    case Direction::right:
        y+=height/2 - 4;
        facing = Direction::right;
        break;
    default:
        break;
    }

    // int difX = point.x-x;
    // int difY = point.y-y;
    // double angle = atan2(difY, difX);
    // dx = cos(angle)*maxSpeed;
    // dy = sin(angle)*maxSpeed;
    if(spawned->facing == Direction::left) {
        dx = -maxSpeed;
        image.setRotation(270);
    } else if(spawned->facing == Direction::right) {
        dx = maxSpeed;
        image.setRotation(90);
    }

    // image.setRotation(90+atan2(dy, dx)*180/PI);
    Gravity = 0;
    image.setPosition(x,y);
}

void Projectile::collide(Agent* smasher) {
    this->undoMove();
}

void Projectile::move(const RenderWindow &relativeTo) {
    if(facing == Direction::left) {
        dx = -maxSpeed;
        image.setRotation(270);
    } else if(facing == Direction::right) {
        dx = maxSpeed;
        image.setRotation(90);
    }

    x += dx/3;
    y += dy/3;

    if(x > 800) {
        x = 800;   
        dx = 0;
    }
    if(x < 0) {
        x = 0;
        dx = 0;
    }
    if(y > 500) {
        y = 500;
        dy = 0;
    }
    if(y < 0) {
        y = 0;
        dy = 0;
    }
    image.setPosition(x,y);
}

void Projectile::update(const RenderWindow &relativeTo) {
    move(relativeTo);
}
