#include"Tile.hpp"

Tile::Tile(float _x, float _y) {
    x = _x;
    y = _y;
    imageStr = "red.png";
    tex = new Texture();
    tex->loadFromFile(imageStr);
    image.setTexture(*tex);
    dx = dy = 0;
    image.setPosition(x,y);

    maxSpeed = 0;
    Gravity = 0;
    MaxGravity = 0;
}
