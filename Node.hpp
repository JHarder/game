#ifndef NODE_H
#define NODE_H

#include"Tile.hpp"

class Node {
public:
  Node(Tile t, Node parent, int gCost, int hCost);
  void setH(int _H);
  void setG(int _G);
  inline void getF() { return G+H }
  inline void setParent(Node *n) { parent = n; }
private:
  Node *parent;
  int H, G;
  int ID;
  static int maxID;
};

#endif // NODE_H
