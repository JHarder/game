#include"Enemy.hpp"

Enemy::Enemy(float _x, float _y) {
  x=_x;
  y=_y;
  state = aggresive;
}

void Enemy::update(const RenderWindow &relativeTo) {
  reactToWorld();
  move(relativeTo);
}

void Enemy::reactToWorld() {
  // how to interact with the world
  // access to player? which parts of the player?
  // pathfinding
  switch(state) {
  case passive:
	// do nothing?
  	break;
  case defensive:
	// run away from player?
	break;
  case aggresive:
	// run to player
	break;
  default:
	break;
  }
}
