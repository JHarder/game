#include"Agent.hpp"
#include"Constants.hpp"

Agent::Agent(float _x, float _y) {
	imageStr = "ship.png";
	tex = new Texture();
	tex->loadFromFile(imageStr);
	image.setTexture(*tex);
	dx = dy = 0;
	x = _x;
	y = _y;
    oldx = x;
    oldy = y;
	image.setPosition(x,y);
	maxSpeed = 30;
	image.setOrigin(16,16);
	// image.setOrigin(_x, _y);
	image.setRotation(90);
    Gravity = GRAVITY;
    MaxGravity = MAX_GRAVITY;
    facing = Direction::right;
}

Sprite Agent::getDrawable() {
	return image;
}

void Agent::move(const RenderWindow &relativeTo) {
    oldx = x;
    oldy = y;

    dy += Gravity;
    if(dy > MaxGravity) {
        dy = MaxGravity;
    }

    if(currentDir == Direction::right) {
        image.setRotation(90);
    }
    if(currentDir == Direction::left) {
        image.setRotation(270);
    }

    x += dx/3;

    // this messes with projectile location relative to player sprite
    y += dy/3;

    if(x > 800) {
        x = 800;   
        dx = 0;
    }
    if(x < 0) {
        x = 0;
        dx = 0;
    }
    if(y > 500) {
        y = 500;
        dy = 0;
    }
    if(y < 0) {
        y = 0;
        dy = 0;
    }
    image.setPosition(x,y);
}

// void Agent::followDirection() {
//     image.setRotation(atan2(dy, dx)*180/PI);
//     image.rotate(90);
// }

void Agent::update(const RenderWindow &relativeTo) {
    move(relativeTo);
}

Vector2i Agent::getCoords() {
    return Vector2i(x,y);
}

void Agent::clash(Agent* clasher){
    Sprite collideSprite = clasher->getDrawable();
    bool hit = checkCollide(collideSprite);
    if(hit) {
        collide(clasher);
        clasher->collide(this);
    }
}

void Agent::collide(Agent* smasher) {
    this->undoMove();
}

bool Agent::checkCollide(Sprite crasher) {
    float myRotation = image.getRotation();
    float otherRotation = crasher.getRotation();
    image.setRotation(0);
    crasher.setRotation(0);
    FloatRect myBounds = image.getGlobalBounds();
    FloatRect otherBounds = crasher.getGlobalBounds();
    image.setRotation(myRotation);
    crasher.setRotation(otherRotation);
    if(myBounds.intersects(otherBounds)) {
        return true;
    } else {
        return false;
    }
}

void Agent::undoMove(){
    x = oldx;
    y = oldy;
    dx = 0;
    // dy = 0;
    image.setPosition(x, y);
}
