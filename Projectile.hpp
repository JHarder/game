#ifndef PROJECTILE_H
#define PROJECTILE_H

#include"Agent.hpp"

class Projectile : public Agent {
private:

public:
    Projectile(Vector2i start, Vector2i point);
    void collide(Agent* smasher);
    void move(const RenderWindow &relativeTo);
    void update(const RenderWindow &relativeTo);
    Projectile(Agent* spawned, Vector2i start);
};

#endif
