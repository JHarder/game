#ifndef _MAP_H
#define _MAP_H

#include"Tile.hpp"
#include<vector>
#include<string>
#include<sstream>
#include<iostream>
#include<fstream>

using namespace std;

vector<Tile*> readMapFile(string file);
bool saveMapFile(string name, vector<Tile*> mapObject);

#endif // _MAP_H
