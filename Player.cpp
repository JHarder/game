#include"Player.hpp"
#include<cmath>

Player::Player(float _x, float _y) {
    x=_x;
    y=_y;
    Agent(x,y);
    dx = 15;
    bulletCooldown = 0;
    coolDownWait = 18;
}

void Player::getInput() {
    if(Keyboard::isKeyPressed(Keyboard::Left) ||
       Keyboard::isKeyPressed(Keyboard::A)) {
        currentDir = facing = Direction::left;
    } else if(Keyboard::isKeyPressed(Keyboard::Right) ||
       Keyboard::isKeyPressed(Keyboard::D)) {
        currentDir = facing = Direction::right;
    } else {
        currentDir = Direction::none;
    }

    if(Keyboard::isKeyPressed(Keyboard::J)) {
        if(bulletCooldown <= 0) {
            bullet = new Projectile(this, Vector2i(x,y));
            bulletCooldown = coolDownWait;
        }
    }

    if(Keyboard::isKeyPressed(Keyboard::Space)) {
        if(y == 500) {
            dy = -30;
        }
    }
}

void Player::applyInput() {
    switch(currentDir) {
    case Direction::left:
        // if(dx > -maxSpeed) {
        //     dx--;
        // }
        dx = -maxSpeed;
        break;
    case Direction::right:
        // if(dx < maxSpeed) {
        //     dx++;
        // }
        dx = maxSpeed;
        break;
    case Direction::none:
        // dx /= 1.5; // decay speed down to 0
        dx = 0;
        break;
    default:
        // do nothing
        break;
    }
}

void Player::collide(Agent* smasher){
    collideBetter(smasher->getDrawable());
}

void Player::collideBetter(Sprite victim) {
    float myRotation = image.getRotation();
    float otherRotation = victim.getRotation();
    image.setRotation(0);
    victim.setRotation(0);
    FloatRect myBounds = image.getGlobalBounds();
    FloatRect otherBounds = victim.getGlobalBounds();

    float left, top, width, height;
    left = myBounds.left;
    top = myBounds.top;
    width = myBounds.width;
    height = myBounds.height;

    image.setPosition(oldx, y);
    FloatRect vertTest = image.getGlobalBounds();
    if(vertTest.intersects(otherBounds)) {
        y = oldy;
        dy = -dy;
    }
    image.setPosition(x, oldy);
    FloatRect horizTest = image.getGlobalBounds();
    if(horizTest.intersects(otherBounds)) {
        x = oldx;
        dx = -dx;
    }

    image.setRotation(myRotation);
    victim.setRotation(otherRotation);
    image.setPosition(x, y);
}

void Player::reset() {
    x = 200;
    y = 50;
    image.setPosition(x,y);
}

void Player::update(const RenderWindow &relativeTo) {
    getInput();
    if(bulletCooldown > 0) {
        bulletCooldown--;
    }
    applyInput();
    move(relativeTo);
}

// void Player::followMouse(const RenderWindow &relativeTo) {
//     Vector2i mousePos = Mouse::getPosition(relativeTo);
//     image.setRotation(atan2(mousePos.y-y, mousePos.x-x)*180/PI);
//     image.rotate(90);
// }
