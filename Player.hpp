#ifndef PLAYER_H
#define PLAYER_H

#include"Agent.hpp"
#include"Projectile.hpp"

class Player : public Agent {
  public:
    Player(float _x, float _y);
    void collide(Agent* smasher);
    void collideBetter(Sprite victim);
	void update(const RenderWindow &relativeTo);
    Projectile *bullet;
    void reset();

  private:
    void getInput();
	void applyInput();
    void followMouse(const RenderWindow &relativeTo);
    int bulletCooldown;
    int coolDownWait;
};

#endif // PLAYER_H
